/**
 * IO Streaming for Private Chat
 * Copyright (C) 2008 @author Naiyuan Zhang
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/.
 */

package IO;

import java.net.*;
import java.io.*;
import java.util.logging.*;
import Graphics.*;

public class IOStream implements Runnable {

    public Socket socket;
    public PrintWriter out = null;
    public BufferedReader in = null;
    public ClientGUI gui;
    public String ip = "default";
    public String port = "default";
    public String clientName = "default";
    public String password = "default";
    public String screenName = "default";
    public String curChannel = "default";
    public String lastestServerMsg = "default";
    public Thread streamThread;
    
    public IOStream(ClientGUI _gui, String _ip, String _port, String _clientName, String _password) {
        gui = _gui;
        ip = _ip;
        port = _port;
        clientName = _clientName;
        password = _password;
        streamThread = new Thread(this);
        streamThread.start();
    }
    
    public void run() {
        login();
        //System.out.println("login complete");
        try {
            while (socket.isConnected()) {
                //System.out.println("login complete3");
                lastestServerMsg = in.readLine();
                System.out.println(lastestServerMsg);
                respondToServer(lastestServerMsg);
                Thread.sleep(2000);
            }
        } catch (IOException ex) {
            Logger.getLogger(IOStream.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(IOStream.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Leaving");
    }
    
    public void respondToServer(String SC) {
        if (SC.startsWith("sudo:")) {
            SC = SC.substring(5);
            if (SC.equals("disconnect")) {
                gui.loginLayer.setVisible(true);
                gui.inLayer.setVisible(false);
                gui.loginStatus.setText("You have been kicked by the server...");
                gui.screeniF.grabFocus();
                disconnect();
            }
        }
    }
    
    public void login() {
        try {
            if (ip.isEmpty() || ip.equals("localhost")) {
                ip = "127.0.0.1";
                gui.serverF.setText(ip);
            }
            if (clientName.isEmpty()) {
                clientName = "BigNoob";
                gui.screeniF.setText("BigNoob");
            }
            if (password.isEmpty()) {
                password = "default";
                gui.passF.setText("default");
            }
            screenName = clientName;
            socket = new Socket(ip, Integer.parseInt(port));
            out = new PrintWriter(socket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            if (login(clientName, password)) {
                gui.loginLayer.setVisible(false);
                gui.inLayer.setVisible(true);
                gui.curNameL.setText(screenName);
                curChannel = "Lobby";
                gui.refreshChanList();
                gui.channelList.setSelectedIndex(0);
            } else {
                disconnect();
            }
        } catch (UnknownHostException ex) {
            Logger.getLogger(ClientGUI.class.getName()).log(Level.SEVERE, null, ex);
            gui.loginStatus.setText("Error connecting to " + ip + ":" + port);
        } catch (IOException ex) {
            Logger.getLogger(ClientGUI.class.getName()).log(Level.SEVERE, null, ex);
            gui.loginStatus.setText("Error connecting to " + ip + ":" + port);
        }
    }
    
    public void disconnect() {
        try {
            out.close();
            in.close();
            socket.close();
        } catch (IOException ex) {
            Logger.getLogger(IOStream.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public boolean login(String clientName, String password) {
        String serverReply = "";
        if (clientName.contains(":") || clientName.contains("-")
                || password.contains(":") || password.contains("-")) {
            gui.setStatus("Invalid characters (: or -) in username or password...");
            return false;
        }
        serverReply = sendToServer("login:" + clientName + "-" + password);
        System.out.println("Reply: " + serverReply);
        if (serverReply.equals("allowed")) {
            return true;
        } else if (serverReply.equals("timed out")) {
            gui.setStatus("Server timed out.");
        } else {
            gui.setStatus("Connection denied.");
        }
        return false;
    }
    
    public String[] getChannels() {
        String serverReply = sendToServer("getChannels");
        System.out.println("Reply: " + serverReply);
        if (serverReply.equals("timed out")) {
            return new String[] {
                "List not available"
            };
        } else {
            return serverReply.split("-");
        }
    }

    public String sendToServer(String text) {
        try {
            String serverReply = "";
            out.println(text);
            Thread.sleep(200);
            if ((serverReply = in.readLine()) == null) {
                return "timed out";
            }
            /*
            while ((serverReply = ClientGUI.gui.in.readLine()) == null) {
                waitTime++;
                if (waitTime > 5000) {
                    return "timed out";
                }
            }*/
            return serverReply;
        } catch (InterruptedException ex) {
            Logger.getLogger(IOHandler.class.getName()).log(Level.SEVERE, null, ex);
            return "timed out";
        } catch (IOException ex) {
            Logger.getLogger(IOHandler.class.getName()).log(Level.SEVERE, null, ex);
            return "timed out";
        }
    }
    
}
