/**
 * IO Handler for Private Chat
 * Copyright (C) 2008 @author Naiyuan Zhang
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/.
 */

package IO;

import java.net.*;
import java.io.*;
import java.util.logging.*;
import Graphics.*;
import java.util.*;

public class IOHandler {
    
    /*
    public IOHandler(ClientGUI _gui) {
        super(_gui);
    }

    public boolean login(String clientName, String password) {
        String serverReply = "";
        if (clientName.contains(":") || clientName.contains("-")
                || password.contains(":") || password.contains("-")) {
            gui.setStatus("Invalid characters (: or -) in username or password...");
            return false;
        }
        serverReply = sendToServer("login:" + clientName + "-" + password);
        System.out.println("Reply: " + serverReply);
        if (serverReply.equals("allowed")) {
            return true;
        } else if (serverReply.equals("timed out")) {
            gui.setStatus("Server timed out.");
        } else {
            gui.setStatus("Connection denied.");
        }
        return false;
    }
    
    public String[] getChannels() {
        String serverReply = sendToServer("getChannels");
        System.out.println("Reply: " + serverReply);
        if (serverReply.equals("timed out")) {
            return new String[] {
                "List not available"
            };
        } else {
            return serverReply.split("-");
        }
    }

    public String sendToServer(String text) {
        try {
            String serverReply = "";
            out.println(text);
            Thread.sleep(200);
            if ((serverReply = in.readLine()) == null) {
                return "timed out";
            }
            /*
            while ((serverReply = ClientGUI.gui.in.readLine()) == null) {
                waitTime++;
                if (waitTime > 5000) {
                    return "timed out";
                }
            }
            return serverReply;
        } catch (InterruptedException ex) {
            Logger.getLogger(IOHandler.class.getName()).log(Level.SEVERE, null, ex);
            return "timed out";
        } catch (IOException ex) {
            Logger.getLogger(IOHandler.class.getName()).log(Level.SEVERE, null, ex);
            return "timed out";
        }
    }
    */
}